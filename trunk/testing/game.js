/*    This file is part of Face Memorizer.
 *
 *    Face Memorizer is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Foobar is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Face Memorizer.  If not, see <http://www.gnu.org/licenses/>.
 */
 
var QuestionBox = Class.create({
  initialize: function(id) {
    this.id = id;
    this.choice_mode = false;
    this.choices = new Array();
  },
  plot: function(parent) {
    template = $$('div.template div.questionboard')[0];
    this.board = template.clone(true);
    parent.appendChild(this.board);
    this.img = this.board.select('img')[0];
    this.open_div = this.board.select('.open_answer')[0];
    this.ainput = this.open_div.select('input')[0];
    this.ainput.name = 'question' + this.id + 'answer';
    this.answer_p = this.board.select('.correct_answer')[0];
    this.multi_div = this.board.select('.choice_answer_area')[0];
  },
  check: function() {
    if(this.choice_mode) {
      this.choices.each(function(choice) {
        var input = choice.select('input')[0];
        input.disable();
        if(input.value == this.face.name) {
          choice.addClassName(input.checked ? 'right_answer' : 'correct_answer');
        } 
        else if(input.checked) {
          choice.addClassName('wrong_answer');
        }
      }, this);
    }
    else {
      this.ainput.disable();
      if (this.ainput.value == this.face.name) {
        this.open_div.addClassName('right_answer');
      }
      else {
        this.open_div.addClassName('wrong_answer');
        this.answer_p.select('span')[0].innerHTML = 
          this.face.name.escapeHTML();
        this.answer_p.show();
      }
    }
  },
  show_choices: function(choices) {
    choices.splice(Math.floor(Math.random() * (choices.length + 1)),
      0, this.face.name);
    var template = $$('div.template div.choice_answer')[0];
    var tag = 'question' + this.id + 'choice';
    choices.each(function(choice, i) {
      var input;
      var label;
      if(!this.choices[i]) {
        this.choices[i] = template.clone(true);
        this.multi_div.appendChild(this.choices[i]);
        input = this.choices[i].select('input')[0];
        label = this.choices[i].select('label')[0];
        input.name = tag;
        input.id = tag + i;
        label.htmlFor = tag + i;
      } else {
        input = this.choices[i].select('input')[0];
        label = this.choices[i].select('label')[0];
      }
      input.value = choice;
      input.checked = false;
      input.enable();
      label.innerHTML = choice.escapeHTML();
      this.choices[i].removeClassName('right_answer');
      this.choices[i].removeClassName('wrong_answer');
      this.choices[i].removeClassName('correct_answer');
    }, this);
    this.choices.splice(choices.length, 
      this.choices.length - choices.length).invoke('remove');
    this.open_div.hide();
    this.multi_div.show();
    this.choice_mode = true;
  },
  setup: function(face) {
    this.face = face;
    this.img.src = this.face.img;
    this.ainput.value = '';
    this.ainput.enable();
    this.answer_p.hide();
    this.multi_div.hide();
    this.open_div.removeClassName('right_answer');
    this.open_div.removeClassName('wrong_answer');
    this.open_div.removeClassName('correct_answer');
    this.open_div.show();
    this.choice_mode = false;
  },
});
    var Game = Class.create({
      initialize: function(num_questions, num_choices, faces_json) {
        this.questions = new Array();
        num_questions.times(function(id) {
          this.questions.push(new QuestionBox(id));
        }, this);
        this.num_choices = num_choices;
        this.faces = faces_json.faces;
      },
      plot_board: function(parent) {
        template = $$('div.template div.gameboard')[0];
        this.board = template.clone(true);
        parent.appendChild(this.board);
        var qdiv = this.board.select('div.question_area')[0];
        this.questions.invoke('plot', qdiv);
        this.check_btn = this.board.select('input[name=check_btn]')[0];
        this.check_btn.observe('click', this.check_answers.bind(this));
        this.choice_btn = this.board.select('input[name=choice_btn]')[0];
        this.choice_btn.observe('click', this.show_choices.bind(this));
        this.next_btn = this.board.select('input[name=next_btn]')[0];
        this.next_btn.observe('click', this.next_question.bind(this));
      },
      check_answers: function() {
        this.check_btn.hide();
        this.choice_btn.hide();
        this.questions.invoke('check');
      },
      show_choices: function() {
        this.choice_btn.hide();
        this.questions.each(function(q) {
          q.show_choices(
            this.random_faces(this.num_choices - 1, [ q.face ]).pluck('name')
          );
        }, this);
      },
      next_question: function() {
        this.random_faces(this.questions.length).each(function(f,i) {
          this.questions[i].setup(f);
        }, this)
        this.check_btn.show();
        this.choice_btn.show();
      },
      random_faces: function(amount, dont_include) {
        if(!dont_include) {
          dont_include = new Array();
        }
        var rfaces = new Array();
        for(var nchoices = 
          Math.min(amount, this.faces.length - dont_include.length); 
          nchoices > 0; --nchoices)
        {
          var index = Math.floor(Math.random() * this.faces.length);
          while(dont_include.include(this.faces[index]) || 
            rfaces.include(this.faces[index]))
          {
            index = (index + 1) % this.faces.length;
          }
          rfaces.push(this.faces[index]);
        }
        return rfaces;
      },
    });

